class AddColumnNameToUsers < ActiveRecord::Migration
  def change
    add_column :users, :name, :string
    add_column :users, :avatar, :string
    add_column :users, :age, :integer
    add_column :users, :website, :string
    add_column :users, :phone, :string
    add_column :users, :adress, :text
  end
end
