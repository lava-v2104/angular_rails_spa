class Article < ActiveRecord::Base
	belongs_to :user
	validates :title, presence: true
	validates :description, length: { in: 3..10 }
end
