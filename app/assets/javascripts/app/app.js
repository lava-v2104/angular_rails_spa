var app = angular.module('resourses',['ngRoute'])

app.config(['$routeProvider',function($routeProvider, $routeParams) {
	$routeProvider
		.when('/', {
			templateUrl: "/home",
			controller: "HomeCtrl"
		})
		.when('/articles', {
			templateUrl: "/articles",
			controller: "ArticlesCtrl"
		})
		.when('/articles/new', {
			templateUrl: "/articles/new",
			controller: "NewArticleCtrl"
		})
		.when('/articles/:id/edit', {
			templateUrl: function(params){ return '/articles/' + params.id +'/edit'; },
			controller: "EditArticleCtrl"
		})
		.when('/articles/:id', {
			templateUrl: function(params){ return '/articles/' + params.id;},
			controller: "ShowArticleCtrl"
		})
		.when('/contacts', {
			templateUrl: "/contacts",
			controller: "ContactsCtrl"
		})
		.when('/users/edit', {
			templateUrl: "/users/edit",
			controller: "UsersEditCtrl"
		})
}])


app.controller('HomeCtrl', ['$rootScope', function($rootScope){
	$rootScope.currentPage = 'home'
	
}])

app.controller('UsersEditCtrl', ['$scope', '$http', '$location', function($scope, $http, $location){
	$scope.formData = {};
	$http({method: 'GET', url: '/user/info'}).success(function (res) {
		$scope.formData.user = res
	})

	$scope.saveUser = function (fd) {
		$http({method: "PUT", url: "/users", data: fd})
			.success(function (res) {
				$location.path('/')
			})
			.error(function (res) {
				$location.path('/')
			})
	}
}])

app.controller('NewArticleCtrl', ['$rootScope', '$location', '$http', '$scope', function($rootScope, $location, $http, $scope){
	$rootScope.currentPage = 'articles';

	$scope.saveArticle = function (fd) {
		angular.element(document.querySelectorAll('.error')).remove();
		$http({method: "POST", url: "/articles.json", data: fd})
			.success(function (res) {
				console.log("OK ",res)
				$location.path('/articles')
			})
			.error(function (res) {
				_.each(res.errors, function (v, k) {
					angular.element(document.querySelector('.'+k)).append('<span class="error">'+v+'</span>')
				})
			})
	}
}])

app.controller('EditArticleCtrl', ['$rootScope', '$location', '$http', '$scope', '$routeParams', function($rootScope, $location, $http, $scope, $routeParams){
	$rootScope.currentPage = 'articles';

	$scope.saveArticle = function (fd) {
		angular.element(document.querySelectorAll('.error')).remove();
		$http({method: "PUT", url: "/articles/"+$routeParams.id+".json", data: fd})
			.success(function (res) {
				$location.path('/articles')
			})
			.error(function (res) {
				_.each(res.errors, function (v, k) {
					angular.element(document.querySelector('.'+k)).append('<span class="error">'+v+'</span>')
				})
			})
	}
}])

app.controller('ShowArticleCtrl', ['$rootScope', '$http', '$routeParams', function($rootScope, $http, $routeParams){
	$rootScope.currentPage = 'articles';
		$http({method: "GET", url: "/articles/"+$routeParams.id})
}])

app.controller('ArticlesCtrl', ['$scope', '$rootScope', '$http', function($scope, $rootScope, $http){
	$rootScope.currentPage = 'articles'

	$scope.doSearch = function (str) {
		$http({method: "GET", url: "/search", params: {q: str}}).success(function (res) {
			$scope.articles = res;
		})
	}

	$scope.remove = function (id) {
		var temp = "/articles/"+id+'.json';
		$http({method: "DELETE", url: temp}).success(function (res) {
			$scope.articles = res;
		})
	}

	$rootScope.$watch('search', function (val) {
		$scope.doSearch(val)
	})
}])

app.controller('ContactsCtrl', ['$rootScope', function($rootScope){
	$rootScope.currentPage = 'contacts'
}])

app.directive('fileread', [function(){
	// Runs during compile
	return {
		// name: '',
		// priority: 1,
		// terminal: true,
		scope: {
			fileread: '='
		}, // {} = isolate, true = child, false/undefined = no change
		// controller: function($scope, $element, $attrs, $transclude) {},
		// require: 'ngModel', // Array = multiple requires, ? = optional, ^ = check parent elements
		// restrict: 'A', // E = Element, A = Attribute, C = Class, M = Comment
		// template: '',
		// templateUrl: '',
		// replace: true,
		// transclude: true,
		// compile: function(tElement, tAttrs, function transclude(function(scope, cloneLinkingFn){ return function linking(scope, elm, attrs){}})),
		link: function($scope, iElm, iAttrs, controller) {
			iElm.bind('change', function (val) {
				$scope.fileread = iElm[0].files;
				$scope.$apply()
			})
		}
	};
}]);