class ApplicationController < ActionController::Base
  before_action :configure_permitted_parameters, if: :devise_controller?

  def layout
    render file: 'layouts/application'
  end

  def user_info
    render text: current_user.to_json(only: [:name, :age, :phone, :website, :adress, :email])
  end

  protected
  def configure_permitted_parameters
    devise_parameter_sanitizer.for(:account_update) { |u| u.permit(:name, :avatar, :age, :website, :phone, :adress, :current_password) }
  end
  protect_from_forgery with: :exception
  layout :false
end
